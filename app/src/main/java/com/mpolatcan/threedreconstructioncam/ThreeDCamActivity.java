package com.mpolatcan.threedreconstructioncam;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Mat;

import java.io.File;
import java.io.IOException;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;

import static edu.cmu.pocketsphinx.SpeechRecognizerSetup.defaultSetup;

/**
 * Created by mpolatcan-gyte_cse on 19.01.2017.
 */

public class ThreeDCamActivity extends AppCompatActivity
        implements CameraBridgeViewBase.CvCameraViewListener2, RecognitionListener{
    private WifiConnectionDialog wifiConnectionDialog;
    private SettingsDialog settingsDialog;
    private Toolbar toolbar;
    private CameraBridgeViewBase camera;
    private Handler handler;
    private Mat frame;
    private StreamingServiceStatusReceiver streamingServiceStatusReceiver;
    private SpeechRecognizer speechRecognizer;
    private MenuItem fpsValue, connectionStatus;
    private static final String OPENCV_LOADER = "OpenCVLoader";
    private static long beginningTime;
    private int frameNum = 0;
    private boolean isConnected = false;
    private boolean isPending = false;

    private String CURRENT_EDIT_TEXT;
    private String LAST_VOICE_COMMAND;

    static {
        if (OpenCVLoader.initDebug()) {
            Log.d(OPENCV_LOADER,"OpenCV successfully loaded!");
        } else {
            Log.d(OPENCV_LOADER,"OpenCV can't be loaded");
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.threedcam_activity);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        toolbar = ((Toolbar) findViewById(R.id.threed_cam_toolbar));
        toolbar.setTitle("3D Reconstruction Camera");
        setSupportActionBar(toolbar);

        camera = ((CameraBridgeViewBase) findViewById(R.id.threed_cam));
        camera.setCvCameraViewListener(this);
        camera.setMaxFrameSize(640,480);

        handler = new Handler();

        wifiConnectionDialog = new WifiConnectionDialog(this);
        settingsDialog = new SettingsDialog(this);

        streamingServiceStatusReceiver = new StreamingServiceStatusReceiver();
        IntentFilter streamingServiceFilter = new IntentFilter();
        streamingServiceFilter.addAction(ThreeDCamConstants.ACTION_CONNECTION_STATUS);
        streamingServiceFilter.addAction(ThreeDCamConstants.ACTION_FRAME_HAS_SENDED);
        registerReceiver(streamingServiceStatusReceiver,streamingServiceFilter);

        // Initialize voice recognition system
//        new VoiceRecognitionInitializer().execute();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onPause() {
        camera.disableView();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera.enableView();

        // Start background service in there
        Intent startStreamerService = new Intent(this,FrameStreamingService.class);
        startStreamerService.setAction(ThreeDCamConstants.ACTION_START_SERVICE);
        startService(startStreamerService);
    }

    @Override
    protected void onStop() {
        camera.disableView();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        camera.disableView();
//        speechRecognizer.cancel();
//        speechRecognizer.shutdown();
        unregisterReceiver(streamingServiceStatusReceiver);
        super.onDestroy();
    }

    @Override
    protected void onRestart() {
        camera.disableView();
        super.onRestart();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.threedcam_menu,menu);

        fpsValue = menu.findItem(R.id.fps_value);
        connectionStatus = menu.findItem(R.id.connect_to_device);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.connect_to_device:
                wifiConnectionDialog.show();
                break;
            case R.id.settings:
                settingsDialog.show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class StreamingServiceStatusReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case ThreeDCamConstants.ACTION_CONNECTION_STATUS:
                    isConnected = intent.getBooleanExtra(ThreeDCamConstants.EXTRA_CONNECTION_STATUS,false);
                    wifiConnectionDialog.setIsConnectionEstablished(isConnected);

                    if (isConnected) {
                        beginningTime = System.currentTimeMillis() / 1000; // get beginning time for calculation fps
                        connectionStatus.setIcon(ContextCompat.getDrawable(ThreeDCamActivity.this,
                                R.drawable.ic_cast_connected_black_24dp));
                    } else {
                        connectionStatus.setIcon(ContextCompat.getDrawable(ThreeDCamActivity.this,
                                R.drawable.ic_cast_black_24dp));
                    }

                    break;
                case ThreeDCamConstants.ACTION_FRAME_HAS_SENDED:
                    isPending = false;

                    ++frameNum;

                    final long elapsedTime = (System.currentTimeMillis() / 1000) - beginningTime;

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            if (elapsedTime != 0)
                                fpsValue.setTitle("FPS: " + frameNum / elapsedTime);
                        }
                    });

                    break;
                default:
                    break;
            }
        }
    }

    // --------------------- CAMERA CALLBACKS -----------------------------------
    @Override
    public void onCameraViewStarted(int width, int height) {

    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        frame = inputFrame.rgba();

        if (isConnected && !isPending) {
            isPending = true;
            FrameStreamingService.currentFrame = frame.clone();
            Intent sendFrame = new Intent(this, FrameStreamingService.class);
            sendFrame.setAction(ThreeDCamConstants.ACTION_SEND_FRAME);
            startService(sendFrame);
        }

//        Log.d("ThreeDCam", "ImageSize: " + frame.total() * frame.elemSize());
//        Log.d("ThreeDCam", "Resolution: " + frame.rows() + "x" + frame.cols());

        return frame;
    }
    // ---------------------------------------------------------------------------

    // -------------------- VOICE RECOGNITION CALLBACKS --------------------------

    private class VoiceRecognitionInitializer extends AsyncTask<Void,Void,Exception> {
        @Override
        protected Exception doInBackground(Void... params) {
            try {
                Assets assets = new Assets(ThreeDCamActivity.this);
                File assetDir = assets.syncAssets();
                setupRecognizer(assetDir);
            } catch (IOException ex) {
                return ex;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Exception result) {
            if (result != null) {
                Toast.makeText(ThreeDCamActivity.this, "" + result, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(ThreeDCamActivity.this, "Voice recognition activated", Toast.LENGTH_LONG).show();
                LAST_VOICE_COMMAND = ThreeDCamConstants.MAIN_VOICE_COMMANDS;
                sendVoiceCommand(LAST_VOICE_COMMAND);
            }
        }

        private void setupRecognizer(File assetsDir) throws IOException{
            speechRecognizer = defaultSetup()
                    .setAcousticModel(new File(assetsDir,"en-us-ptm"))
                    .setDictionary(new File(assetsDir,"cmudict-en-us.dict"))

                    // To disable logging of raw audio comment out this call (takes a lot of space
                    // on the device)
                    .setRawLogDir(assetsDir)

                    // Threshold to tune for keyphrase the balance between false alarms and misses
                    .setKeywordThreshold(1e-20f)

                    // Use context-independent phonetic search, context-dependent is too slow for mobile
                    .setBoolean("-allphone_ci", true)
                    .getRecognizer();

            speechRecognizer.addListener(ThreeDCamActivity.this);


            File mainVoiceCommands = new File(assetsDir, "main_voice_commands.gram");
            speechRecognizer.addGrammarSearch(ThreeDCamConstants.MAIN_VOICE_COMMANDS,mainVoiceCommands);

            File digitsVoiceCommands = new File(assetsDir, "digits_voice_commands.gram");
            speechRecognizer.addGrammarSearch(ThreeDCamConstants.DIGITS_VOICE_COMMANDS,digitsVoiceCommands);

            File connectionVoiceCommands = new File(assetsDir, "connection_voice_commands.gram");
            speechRecognizer.addGrammarSearch(ThreeDCamConstants.CONNECTION_VOICE_COMMANDS,connectionVoiceCommands);

            File settingsVoiceCommands = new File(assetsDir, "settings_voice_commands.gram");
            speechRecognizer.addGrammarSearch(ThreeDCamConstants.SETTINGS_VOICE_COMMANDS,settingsVoiceCommands);
        }
    }

    private void sendVoiceCommand(final String searchName) {
        speechRecognizer.stop();
        speechRecognizer.startListening(searchName);
    }

    @Override
    public void onBeginningOfSpeech() {

    }

    @Override
    public void onEndOfSpeech() {

    }

    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        String speech = hypothesis.getHypstr();

        Log.d("VoiceRecognition", "Speech: " + speech);

        if (speech.equals(ThreeDCamConstants.CONNECTION_VOICE_COMMANDS)) {
            LAST_VOICE_COMMAND = ThreeDCamConstants.CONNECTION_VOICE_COMMANDS;
            wifiConnectionDialog.show();
        } else if (speech.equals(ThreeDCamConstants.SETTINGS_VOICE_COMMANDS)) {
            LAST_VOICE_COMMAND = ThreeDCamConstants.SETTINGS_VOICE_COMMANDS;
            settingsDialog.show();
        } else if (speech.equals(ThreeDCamConstants.CANCEL_COMMAND)) {
            LAST_VOICE_COMMAND = ThreeDCamConstants.MAIN_VOICE_COMMANDS;

            if (wifiConnectionDialog.isShowing())
                wifiConnectionDialog.dismiss();

            if (settingsDialog.isShowing())
                settingsDialog.dismiss();
        } else if (speech.equals(ThreeDCamConstants.OKAY_COMMAND)) {
            LAST_VOICE_COMMAND = ThreeDCamConstants.CONNECTION_VOICE_COMMANDS;
        } else if (speech.equals(ThreeDCamConstants.CONNECTION_ENTER_IP_COMMAND)) {
            LAST_VOICE_COMMAND = ThreeDCamConstants.DIGITS_VOICE_COMMANDS;
            CURRENT_EDIT_TEXT = ThreeDCamConstants.IP_EDIT_TEXT;
            wifiConnectionDialog.enterDigitToIpFieldViaVoiceRecognition("");
        } else if (speech.equals(ThreeDCamConstants.CONNECTION_ENTER_PORT_COMMAND)) {
            LAST_VOICE_COMMAND = ThreeDCamConstants.DIGITS_VOICE_COMMANDS;
            CURRENT_EDIT_TEXT = ThreeDCamConstants.PORT_EDIT_TEXT;
            wifiConnectionDialog.enterDigitToPortFieldViaVoiceRecognition("");
        } else if (isDigit(speech)) {
            if (CURRENT_EDIT_TEXT.equals(ThreeDCamConstants.IP_EDIT_TEXT)) {
                wifiConnectionDialog.enterDigitToIpFieldViaVoiceRecognition(getDigit(speech,CURRENT_EDIT_TEXT));
            } else if (CURRENT_EDIT_TEXT.equals(ThreeDCamConstants.PORT_EDIT_TEXT)) {
                if (getDigit(speech, CURRENT_EDIT_TEXT) != null) {
                    wifiConnectionDialog.enterDigitToPortFieldViaVoiceRecognition(getDigit(speech, CURRENT_EDIT_TEXT));
                }
            }
        }

        sendVoiceCommand(LAST_VOICE_COMMAND);
    }

    @Override
    public void onResult(Hypothesis hypothesis) {

    }

    @Override
    public void onError(Exception e) {
    }

    @Override
    public void onTimeout() {
        sendVoiceCommand(LAST_VOICE_COMMAND);
    }

    private boolean isDigit(String speech) {
        if (speech.equals(ThreeDCamConstants.DIGIT_ZERO) ||
            speech.equals(ThreeDCamConstants.DIGIT_ONE) ||
            speech.equals(ThreeDCamConstants.DIGIT_TWO) ||
            speech.equals(ThreeDCamConstants.DIGIT_THREE) ||
            speech.equals(ThreeDCamConstants.DIGIT_FOUR) ||
            speech.equals(ThreeDCamConstants.DIGIT_FIVE) ||
            speech.equals(ThreeDCamConstants.DIGIT_SIX) ||
            speech.equals(ThreeDCamConstants.DIGIT_SEVEN) ||
            speech.equals(ThreeDCamConstants.DIGIT_EIGHT) ||
            speech.equals(ThreeDCamConstants.DIGIT_NINE) ||
            speech.equals(ThreeDCamConstants.DIGIT_DOT)) {
            return true;
        } else {
            return false;
        }
    }

    private String getDigit(String speech, String editText) {
        switch (speech) {
            case ThreeDCamConstants.DIGIT_ZERO:
                return "0";

            case ThreeDCamConstants.DIGIT_ONE:
                return "1";

            case ThreeDCamConstants.DIGIT_TWO:
                return "2";

            case ThreeDCamConstants.DIGIT_THREE:
                return "3";

            case ThreeDCamConstants.DIGIT_FOUR:
                return "4";

            case ThreeDCamConstants.DIGIT_FIVE:
                return "5";

            case ThreeDCamConstants.DIGIT_SIX:
                return "6";

            case ThreeDCamConstants.DIGIT_SEVEN:
                return "7";

            case ThreeDCamConstants.DIGIT_EIGHT:
                return "8";

            case ThreeDCamConstants.DIGIT_NINE:
                return "9";

            case ThreeDCamConstants.DIGIT_DOT:
                if (CURRENT_EDIT_TEXT.equals(ThreeDCamConstants.IP_EDIT_TEXT)) {
                    return ".";
                } else {
                    return null;
                }

            default:
                return null;
        }
    }
    //--------------------------------------------------------------------------------
}
