package com.mpolatcan.threedreconstructioncam;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfInt;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.net.Socket;

public class FrameStreamingService extends Service {
    public static int compressionType;
    public static int quality;
    public static Mat currentFrame;
    private Socket dataSocket,
                   dataSizeSocket;
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        if (intent != null) {
            if (intent.getAction().equals(ThreeDCamConstants.ACTION_SEND_FRAME)) {
               new Thread(new Runnable() {
                   @Override
                   public void run() {
                       try {
                           Imgproc.cvtColor(currentFrame,currentFrame,Imgproc.COLOR_BGRA2RGB);
                           currentFrame.reshape(0,1);

                           MatOfByte encodedMat = new MatOfByte();
                           MatOfInt encodingParams = new MatOfInt(compressionType,quality);
                           Imgcodecs.imencode(".jpg",currentFrame,encodedMat,encodingParams);
                           Log.d("FSS", "Encoded data size: " + encodedMat.toArray().length);

                           byte[] response = new byte[11];

                           // Send compressed data size
                           dataSizeSocket.getOutputStream().write(
                                   Integer.toString(encodedMat.toArray().length).getBytes());
                           dataSizeSocket.getInputStream().read(response);
//                           Log.d("ServerResponse", "" + new String(response));
                           // ---------------------------------------------

                           // Send compressed data
                           dataSocket.getOutputStream().write(encodedMat.toArray());
                       } catch (Exception ex) {
                           ex.printStackTrace();
                       } finally {
                           Intent frameHasSended = new Intent(ThreeDCamConstants.ACTION_FRAME_HAS_SENDED);
                           sendBroadcast(frameHasSended);
                       }
                   }
               }).start();
            } else if (intent.getAction().equals(ThreeDCamConstants.ACTION_CONNECT)) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String ip = intent.getStringExtra(ThreeDCamConstants.EXTRA_IP);
                        int port = intent.getIntExtra(ThreeDCamConstants.EXTRA_PORT,0);

//                        Log.d("StreamingService", "Ip: " + ip);
//                        Log.d("StreamingService", "Port: " + port);

                        try {
                            dataSocket = new Socket(ip,port);
                            dataSizeSocket = new Socket(ip,port);

                            dataSocket.setSoTimeout(60000);
                            dataSizeSocket.setSoTimeout(60000);

                            Intent connectionEstablished = new Intent(ThreeDCamConstants.ACTION_CONNECTION_STATUS);
                            connectionEstablished.putExtra(ThreeDCamConstants.EXTRA_CONNECTION_STATUS,true);
                            sendBroadcast(connectionEstablished);
                        } catch (Exception ex) {
                            Log.d("FrameStreamingService", "" + ex);

                            Intent connectionCantEstablished = new Intent(ThreeDCamConstants.ACTION_CONNECTION_STATUS);
                            connectionCantEstablished.putExtra(ThreeDCamConstants.EXTRA_CONNECTION_STATUS,false);
                            sendBroadcast(connectionCantEstablished);
                        }
                    }
                }).start();
            } else if (intent.getAction().equals(ThreeDCamConstants.ACTION_DISCONNECT)) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Intent connectionEnded = new Intent(ThreeDCamConstants.ACTION_CONNECTION_STATUS);
                            connectionEnded.putExtra(ThreeDCamConstants.EXTRA_CONNECTION_STATUS,false);
                            sendBroadcast(connectionEnded);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }).start();
            }
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
