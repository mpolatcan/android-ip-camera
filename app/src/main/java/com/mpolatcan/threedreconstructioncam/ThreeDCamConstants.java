package com.mpolatcan.threedreconstructioncam;

/**
 * Created by mpolatcan-gyte_cse on 01.02.2017.
 */

public interface ThreeDCamConstants {
    String ACTION_START_SERVICE = "ACTION_START_SERVICE";
    String ACTION_SEND_FRAME = "ACTION_SEND_FRAME";
    String ACTION_CONNECT = "ACTION_CONNECT";
    String ACTION_DISCONNECT = "ACTION_DISCONNECT";
    String ACTION_CONNECTION_STATUS = "ACTION_CONNECTION_STATUS";
    String ACTION_FRAME_HAS_SENDED = "ACTION_FRAME_HAS_SENDED";
    String EXTRA_CONNECTION_STATUS = "EXTRA_CONNECTION_STATUS";
    String EXTRA_IP = "EXTRA_IP";
    String EXTRA_PORT = "EXTRA_PORT";

    // ---------- VOICE RECOGNITION CONSTANTS -----------
    String MAIN_VOICE_COMMANDS = "main";
    String DIGITS_VOICE_COMMANDS = "digits";
    String CONNECTION_VOICE_COMMANDS = "connection";
    String SETTINGS_VOICE_COMMANDS = "settings";
    String CANCEL_COMMAND = "cancel";
    String OKAY_COMMAND = "okay";
    String CONNECTION_ENTER_IP_COMMAND = "ip";
    String CONNECTION_ENTER_PORT_COMMAND = "port";
    String CONNECTION_CONNECT_COMMAND = "connect";
    String SETTINGS_APPLY_COMMAND = "apply";
    String DIGIT_ZERO = "zero";
    String DIGIT_ONE = "one";
    String DIGIT_TWO = "two";
    String DIGIT_THREE = "three";
    String DIGIT_FOUR = "four";
    String DIGIT_FIVE = "five";
    String DIGIT_SIX = "six";
    String DIGIT_SEVEN = "seven";
    String DIGIT_EIGHT = "eight";
    String DIGIT_NINE = "nine";
    String DIGIT_DOT = "dot";
    String IP_EDIT_TEXT = "IP_EDIT_TEXT";
    String PORT_EDIT_TEXT = "PORT_EDIT_TEXT";
    // ---------------------------------------------------
}
