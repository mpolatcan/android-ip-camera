package com.mpolatcan.threedreconstructioncam;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.opencv.imgcodecs.Imgcodecs;

import java.util.ArrayList;


/**
 * Created by mpolatcan-gyte_cse on 07.02.2017.
 */

// TODO Voice recognition (Try Android SpeechRecognizer and CMUSphinx (PocketSphinx))
public class SettingsDialog extends Dialog {
    private Context context;
    private Button cancelBtn;
    private Button applyBtn;
    private Spinner compressionTypesSpinner;
    private SeekBar frameQualitiesSeekbar;
    private TextView frameQualityVal;
    private static final String COMPRESSION_TYPE_JPEG = "JPEG",
                                COMPRESSION_TYPE_PNG = "PNG",
                                COMPRESSION_TYPE_WEBP = "WEBP";
    private ArrayList<String> compressionTypes;

    public SettingsDialog(Context context) {
        super(context);

        this.context = context;

        compressionTypes = new ArrayList<>();
        compressionTypes.add(COMPRESSION_TYPE_JPEG);
        compressionTypes.add(COMPRESSION_TYPE_PNG);
        compressionTypes.add(COMPRESSION_TYPE_WEBP);

        setContentView(R.layout.settings_dialog);

        cancelBtn = ((Button) findViewById(R.id.settings_dialog_cancel_btn));
        applyBtn = ((Button) findViewById(R.id.settings_dialog_apply_btn));
        compressionTypesSpinner = ((Spinner) findViewById(R.id.compression_types_spinner));
        frameQualitiesSeekbar = ((SeekBar) findViewById(R.id.frame_quality_seekbar));
        frameQualityVal = ((TextView) findViewById(R.id.frame_quality_val));

        setupDialog();
    }

    private void setupDialog() {
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        applyBtn.setEnabled(false);
        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String compressionType = ((String) compressionTypesSpinner.getSelectedItem());

                switch (compressionType) {
                    case COMPRESSION_TYPE_JPEG:
                        FrameStreamingService.compressionType = Imgcodecs.IMWRITE_JPEG_QUALITY;
                        break;

                    case COMPRESSION_TYPE_PNG:
                        FrameStreamingService.compressionType = Imgcodecs.IMWRITE_PNG_COMPRESSION;
                        break;

                    case COMPRESSION_TYPE_WEBP:
                        FrameStreamingService.compressionType = Imgcodecs.IMWRITE_WEBP_QUALITY;
                        break;

                    default:
                        break;
                }

                FrameStreamingService.quality = frameQualitiesSeekbar.getProgress();

                applyBtn.setEnabled(false);
                applyBtn.setTextColor(ContextCompat.getColor(context,android.R.color.darker_gray));
            }
        });

        compressionTypesSpinner.setAdapter(new CustomSpinnerAdapter(context,R.layout.spinner_item,
                compressionTypes));
        compressionTypesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // When compression type change update seekbar's max val
                if (position == 1)
                {
                    frameQualitiesSeekbar.setProgress(0); // Reset progress
                    frameQualitiesSeekbar.setMax(9); // PNG Compression Quality Interval -> 0-9
                } else {
                    frameQualitiesSeekbar.setProgress(0); // Reset progress
                    frameQualitiesSeekbar.setMax(100); // JPEG and WEBP Compression Quality Interval -> 0-100
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        frameQualitiesSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress > 0) {
                    applyBtn.setEnabled(true);
                    applyBtn.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                } else {
                    applyBtn.setEnabled(false);
                    applyBtn.setTextColor(ContextCompat.getColor(context,android.R.color.darker_gray));
                }

                frameQualityVal.setText(Integer.toString(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    private class CustomSpinnerAdapter extends ArrayAdapter<String> {
        private ArrayList<String> datas;
        private Context context;

        public CustomSpinnerAdapter(Context context, int resourceId, ArrayList<String> datas) {
            super(context,resourceId,datas);
            this.context = context;
            this.datas = datas;
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomItem(parent, position, true);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomItem(parent, position, false);
        }

        private View getCustomItem(ViewGroup parent, int position, boolean isDropdownItem) {
            View customItem = LayoutInflater.from(context).inflate(R.layout.spinner_item,parent,false);
            TextView spinnerItemVal = ((TextView) customItem.findViewById(R.id.spinner_item_val));
            spinnerItemVal.setText(datas.get(position));

            if (isDropdownItem) {
                spinnerItemVal.setTextColor(ContextCompat.getColorStateList(context,R.color.spinner_item_text_color));
                spinnerItemVal.setBackground(ContextCompat.getDrawable(context,R.drawable.spinner_item_background));
            } else {
                spinnerItemVal.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
                spinnerItemVal.setBackground(ContextCompat.getDrawable(context,android.R.color.white));
            }

            return customItem;
        }

        @Override
        public int getCount() {
            return datas.size();
        }
    }
}
