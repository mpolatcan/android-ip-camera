package com.mpolatcan.threedreconstructioncam;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by mpolatcan-gyte_cse on 28.01.2017.
 */

// TODO Uptime chronometer
public class WifiConnectionDialog extends Dialog {
    private Activity activity;
    private EditText infoPanelIpField;
    private EditText infoPanelPortField;
    private Button infoPanelCancelBtn;
    private Button infoPanelConnectBtn;
    private Button statusPanelCancelBtn;
    private Button statusPanelDisconnectBtn;
    private RelativeLayout wifiConnectionInfoPanel;
    private RelativeLayout wifiConnectionLoadingPanel;
    private RelativeLayout wifiConnectionStatusPanel;
    private ImageView infoPanelIpValidationMark;
    private ImageView infoPanelPortValidationMark;
    private ImageView loadingPanelConnectionStatusMark;
    private TextView loadingPanelLoadingInfo;
    private TextView statusPanelIpVal;
    private TextView statusPanelPortVal;
    private Handler handler;
    private ProgressBar loadingPanelProgressBar;
    private Animation fadeIn;
    private Animation fadeOut;
    private boolean ipCorrect = false;
    private boolean portCorrect = false;
    private boolean isConnectionEstablished = false;

    public WifiConnectionDialog(Activity activity) {
        super(activity);

        this.activity = activity;

        handler = new Handler();

        setContentView(R.layout.wifi_connection_dialog);

        infoPanelIpField = ((EditText) findViewById(R.id.wifi_connection_info_panel_ip_field));
        infoPanelPortField = ((EditText) findViewById(R.id.wifi_connection_info_panel_port_field));
        infoPanelCancelBtn = ((Button) findViewById(R.id.wifi_connection_info_panel_cancel_btn));
        infoPanelConnectBtn = ((Button) findViewById(R.id.wifi_connection_info_panel_connect_btn));
        wifiConnectionInfoPanel = ((RelativeLayout) findViewById(R.id.wifi_connection_info_panel));
        wifiConnectionLoadingPanel = ((RelativeLayout) findViewById(R.id.wifi_connection_loading_panel));
        wifiConnectionStatusPanel = ((RelativeLayout) findViewById(R.id.wifi_connection_status_panel));
        loadingPanelProgressBar = ((ProgressBar) findViewById(R.id.wifi_connection_loading_panel_progress_bar));
        infoPanelIpValidationMark = ((ImageView) findViewById(R.id.wifi_connection_info_panel_ip_validation_mark));
        infoPanelPortValidationMark = ((ImageView) findViewById(R.id.wifi_connection_info_panel_port_validation_mark));
        loadingPanelConnectionStatusMark = ((ImageView) findViewById(R.id.wifi_connection_loading_panel_status_mark));
        loadingPanelLoadingInfo = ((TextView) findViewById(R.id.wifi_connection_loading_panel_info));
        statusPanelCancelBtn = ((Button) findViewById(R.id.wifi_connection_status_panel_cancel_btn));
        statusPanelDisconnectBtn = ((Button) findViewById(R.id.wifi_connection_status_panel_disconnect_btn));
        statusPanelIpVal = ((TextView) findViewById(R.id.wifi_connection_status_panel_ip_value));
        statusPanelPortVal = ((TextView) findViewById(R.id.wifi_connection_status_panel_port_value));

        infoPanelConnectBtn.setTextColor(ContextCompat.getColor(activity,android.R.color.darker_gray));

        fadeIn = AnimationUtils.loadAnimation(activity,R.anim.fade_in);
        fadeOut = AnimationUtils.loadAnimation(activity,R.anim.fade_out);

        setupDialog();
    }

    public void setIsConnectionEstablished(boolean isConnectionEstablished) {
        this.isConnectionEstablished = isConnectionEstablished;
    }

    public void enterDigitToIpFieldViaVoiceRecognition(String digit) {
        if (!infoPanelIpField.isFocused())
            infoPanelIpField.requestFocus();

        infoPanelIpField.setText(infoPanelIpField.getText() + digit);
    }

    public void enterDigitToPortFieldViaVoiceRecognition(String digit) {
        if (!infoPanelPortField.isFocused())
            infoPanelPortField.requestFocus();

        infoPanelPortField.setText(infoPanelPortField.getText() + digit);
    }

    private void setupDialog() {
        infoPanelIpField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String currentText = infoPanelIpField.getText().toString();

               if (currentText.equals("")) {
                   infoPanelIpValidationMark.setImageDrawable(ContextCompat.getDrawable(activity,
                           android.R.drawable.screen_background_light_transparent));
                   ipCorrect = false;
               } else {
                   new IPChecker(activity).execute(currentText);
               }

               isConnectable();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        infoPanelPortField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (infoPanelPortField.getText().toString().equals("")) {
                    infoPanelPortValidationMark.setImageDrawable(ContextCompat.getDrawable(activity,
                            android.R.drawable.screen_background_light_transparent));
                    portCorrect = false;
                } else {
                    int portInput = Integer.parseInt(infoPanelPortField.getText().toString());

                    if (portInput > 0 && portInput < 65536) {
                        infoPanelPortValidationMark.setImageDrawable(ContextCompat.getDrawable(activity,
                            R.drawable.ic_correct_black_24dp));
                        portCorrect = true;
                    } else {
                        infoPanelPortValidationMark.setImageDrawable(ContextCompat.getDrawable(activity,
                            R.drawable.ic_incorrect_black_24dp));
                        portCorrect = false;
                    }
                }

                isConnectable();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        infoPanelCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        infoPanelConnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                infoPanelConnectBtn.setEnabled(false); // prevent press connect button repeatedly when
                                                       // connecting to remote device
                loadingPanelLoadingInfo.setText("Connecting to Device...");
                loadingPanelProgressBar.setVisibility(View.VISIBLE);
                loadingPanelConnectionStatusMark.setVisibility(View.INVISIBLE);

                wifiConnectionLoadingPanel.setVisibility(View.VISIBLE);

                // Request background service to connect to device and send required information to
                // it
                Intent connectToDevice = new Intent(activity,FrameStreamingService.class);
                connectToDevice.setAction(ThreeDCamConstants.ACTION_CONNECT);
                connectToDevice.putExtra(ThreeDCamConstants.EXTRA_IP,
                                         infoPanelIpField.getText().toString());
                connectToDevice.putExtra(ThreeDCamConstants.EXTRA_PORT,
                                         Integer.parseInt(infoPanelPortField.getText().toString()));
                activity.startService(connectToDevice);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        loadingPanelProgressBar.startAnimation(fadeIn);
                        loadingPanelProgressBar.setVisibility(View.INVISIBLE);

                        loadingPanelLoadingInfo.startAnimation(fadeIn);
                        loadingPanelLoadingInfo.setVisibility(View.INVISIBLE);

                        if (isConnectionEstablished) {
                            loadingPanelConnectionStatusMark.setBackground(ContextCompat.getDrawable(activity,
                                    R.drawable.ic_correct_black_24dp));
                            loadingPanelLoadingInfo.setText("Connected!");
                        } else {
                            loadingPanelConnectionStatusMark.setBackground(ContextCompat.getDrawable(activity,
                                    R.drawable.ic_incorrect_black_24dp));
                            loadingPanelLoadingInfo.setText("Couldn't connected!");
                        }

                        loadingPanelConnectionStatusMark.startAnimation(fadeOut);
                        loadingPanelConnectionStatusMark.setVisibility(View.VISIBLE);

                        loadingPanelLoadingInfo.startAnimation(fadeOut);
                        loadingPanelLoadingInfo.setVisibility(View.VISIBLE);
                    }
                }, 700);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        wifiConnectionLoadingPanel.startAnimation(fadeIn);
                        wifiConnectionLoadingPanel.setVisibility(View.GONE);
                        infoPanelConnectBtn.setEnabled(true);

                        if (isConnectionEstablished) {
                            infoPanelConnectBtn.setEnabled(false);

                            wifiConnectionInfoPanel.startAnimation(fadeIn);
                            wifiConnectionInfoPanel.setVisibility(View.INVISIBLE);

                            // Show infos of connected remote device and uptime
                            statusPanelIpVal.setText(infoPanelIpField.getText().toString());
                            statusPanelPortVal.setText(infoPanelPortField.getText().toString());

                            wifiConnectionStatusPanel.startAnimation(fadeOut);
                            wifiConnectionStatusPanel.setVisibility(View.VISIBLE);
                            statusPanelDisconnectBtn.setEnabled(true);
                            dismiss();
                        }
                    }
                }, 3000);
            }
        });

        statusPanelCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        statusPanelDisconnectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                statusPanelDisconnectBtn.setEnabled(false);
                loadingPanelProgressBar.setVisibility(View.VISIBLE);
                loadingPanelLoadingInfo.setText("Disconnecting...");
                loadingPanelConnectionStatusMark.setVisibility(View.INVISIBLE);

                wifiConnectionLoadingPanel.setVisibility(View.VISIBLE);

                Intent disconnectFromDevice = new Intent(activity,FrameStreamingService.class);
                disconnectFromDevice.setAction(ThreeDCamConstants.ACTION_DISCONNECT);
                activity.startService(disconnectFromDevice);

                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        wifiConnectionLoadingPanel.setVisibility(View.GONE);

                        wifiConnectionStatusPanel.startAnimation(fadeIn);
                        wifiConnectionStatusPanel.setVisibility(View.GONE);

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // Clear old inputs
                                infoPanelIpField.setText("");
                                infoPanelPortField.setText("");

                                wifiConnectionInfoPanel.startAnimation(fadeOut);
                                wifiConnectionInfoPanel.setVisibility(View.VISIBLE);
                            }
                        }, 300);
                    }
                }, 700);
            }
        });
    }

    private void isConnectable() {
        if (ipCorrect && portCorrect) {
            infoPanelConnectBtn.setEnabled(true);
            infoPanelConnectBtn.setTextColor(ContextCompat.getColor(activity,R.color.colorPrimary));
        } else {
            infoPanelConnectBtn.setEnabled(false);
            infoPanelConnectBtn.setTextColor(ContextCompat.getColor(activity,android.R.color.darker_gray));
        }
    }

    private class IPChecker extends AsyncTask<String,Boolean,Boolean> {
        private Activity activity;

        public IPChecker(Activity context) {
            this.activity = context;
        }

        @Override
        protected Boolean doInBackground(String... params) {
            return checkIpV4Address(params[0]);
        }

        @Override
        protected void onPostExecute(final Boolean isIpV4) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isIpV4) {
                        infoPanelIpValidationMark.setImageDrawable(ContextCompat.getDrawable(activity,
                                R.drawable.ic_correct_black_24dp));
                        ipCorrect = true;
                    } else {
                        infoPanelIpValidationMark.setImageDrawable(ContextCompat.getDrawable(activity,
                                R.drawable.ic_incorrect_black_24dp));
                        ipCorrect = false;
                    }
                }
            });
        }

        private boolean checkIpV4Address(final String ipAddress) {
            boolean isIPv4Address;

            try {
                final InetAddress inet = InetAddress.getByName(ipAddress);
                isIPv4Address = inet.getHostAddress().equals(ipAddress) && inet instanceof Inet4Address;
            } catch (final UnknownHostException e) {
                isIPv4Address = false;
            }
            return isIPv4Address;
        }
    }

}
